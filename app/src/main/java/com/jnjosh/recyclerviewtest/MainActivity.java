package com.jnjosh.recyclerviewtest;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class MainActivity extends Activity {
    private static final String TranslationY = "translationY";

    @InjectView(com.jnjosh.recyclerviewtest.R.id.recyclerview) RecyclerView recyclerView;
    @InjectView(com.jnjosh.recyclerviewtest.R.id.fab_container) ViewGroup fabContainer;
    @InjectView(com.jnjosh.recyclerviewtest.R.id.fab) ImageButton fab;
    @InjectView(com.jnjosh.recyclerviewtest.R.id.fab_action_1) ImageButton fab1;
    @InjectView(com.jnjosh.recyclerviewtest.R.id.fab_action_2) ImageButton fab2;
    @InjectView(com.jnjosh.recyclerviewtest.R.id.fab_action_3) ImageButton fab3;

    private boolean expanded = false;
    private float offset1;
    private float offset2;
    private float offset3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.jnjosh.recyclerviewtest.R.layout.activity_main);
        ButterKnife.inject(this);

        recyclerView.setAdapter(LargeAdapter.newInstance(this));
        recyclerView.setLayoutManager(new ScrollingLinearLayoutManager(this, LinearLayoutManager.VERTICAL, false, 5));

        fabContainer.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                fabContainer.getViewTreeObserver().removeOnPreDrawListener(this);
                offset1 = fab.getY() - fab1.getY();
                fab1.setTranslationY(offset1);
                offset2 = fab.getY() - fab2.getY();
                fab2.setTranslationY(offset2);
                offset3 = fab.getY() - fab3.getY();
                fab3.setTranslationY(offset3);
                return true;
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(com.jnjosh.recyclerviewtest.R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == com.jnjosh.recyclerviewtest.R.id.action_top) {
            recyclerView.smoothScrollToPosition(0);
            return true;
        } else if (id == com.jnjosh.recyclerviewtest.R.id.action_bottom) {
            recyclerView.smoothScrollToPosition(recyclerView.getAdapter().getItemCount());

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("unused")
    @OnClick(com.jnjosh.recyclerviewtest.R.id.fab)
    public void buttonTapped(ImageButton fab) {
        expanded = !expanded;
        if (expanded) {
            this.expandFab();
        }
        else {
            this.collapseFab();
        }
    }


    public void fabAction1(View view) {
        Log.i("fab", "fab 1 tapped");
    }


    public void fabAction2(View view) {
        Log.i("fab", "fab 2 tapped");
    }


    public void fabAction3(View view) {
        Log.i("fab", "fab 3 tapped");
    }


    private void expandFab() {
        fab.setImageResource(com.jnjosh.recyclerviewtest.R.drawable.animated_plus);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(createExpandAnimator(fab1, offset1), createExpandAnimator(fab2, offset2), createExpandAnimator(fab3, offset3));
        animatorSet.start();
        animateFab();
    }


    private void collapseFab() {
        fab.setImageResource(com.jnjosh.recyclerviewtest.R.drawable.animated_minus);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(createCollapseAnimator(fab1, offset1), createCollapseAnimator(fab2, offset2), createCollapseAnimator(fab3, offset3));
        animatorSet.start();
        animateFab();
    }


    private Animator createExpandAnimator(View view, float offset) {
        return ObjectAnimator.ofFloat(view, TranslationY, offset, 0).setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime));
    }


    private Animator createCollapseAnimator(View view, float offset) {
        return ObjectAnimator.ofFloat(view, TranslationY, 0, offset).setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime));
    }


    private void animateFab() {
        Drawable drawable = fab.getDrawable();
        if (drawable instanceof Animatable) {
            ((Animatable)drawable).start();
        }
    }

}
